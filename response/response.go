package response

import (
	"gitee.com/guohao88/blog/exception"
	"github.com/gin-gonic/gin"
	"net/http"
)

// 统一处理http返回
// c.JSON(http.StatusBadRequest, err.Error())
// 正常请求数据返回
func Success(c *gin.Context, data any) {
	c.JSON(http.StatusOK, data)
}

// 异常情况的数据返回, 返回我们的业务Exception
func Failed(c *gin.Context, err error) {
	//如果出现多个handler 需要通过手动Abort
	defer c.Abort()

	var e *exception.ApiException
	if v, ok := err.(*exception.ApiException); ok {
		e = v
	} else {
		// 非可以预期, 没有定义业务的情况
		e = exception.New(http.StatusInternalServerError, err.Error())
		e.HttpCode = http.StatusInternalServerError
	}

	c.JSON(e.HttpCode, e)
}
