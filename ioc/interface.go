package ioc

// 定义注册进来的对象的约束条件
//ioc程序有两个阶段，第一对象注册阶段，第二对象初始化阶段
type IocObject interface {
	// 对象的初始化
	Init() error
	// 对象名称
	Name() string
}
