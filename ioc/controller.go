package ioc

// 返回默认的对象注册表
func Controller() *IocContainter {
	return controllerContainer
}

// ioc 注册表对象，全局只有一个
var controllerContainer = &IocContainter{
	store: map[string]IocObject{},
}
