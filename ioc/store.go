package ioc

import "github.com/gin-gonic/gin"

// 定义接口 就是定义逻辑
// 定义一个对象的注册表，IocContainter
type IocContainter struct {
	// 采用Map来保持对象注册
	store map[string]IocObject
}

// Init
// 负责初始化所有的对象，因为map是无序的，这里如果要控制初始化的顺序，可以替换成list
func (c *IocContainter) Init() error {
	for _, obj := range c.store {
		if err := obj.Init(); err != nil {
			return err
		}
	}
	return nil
}

// Registry
//
//	将对象注册到ioc中
func (c *IocContainter) Registry(obj IocObject) {
	c.store[obj.Name()] = obj
}

// Get
// 获取对象，从ioc容器里面获取对象
func (c *IocContainter) Get(name string) any {
	return c.store[name]
}

// Gin
// gin api的接口，用于注册路由
type GinApiHandler interface {
	Registry(r gin.IRouter)
}

// 管理者所有的对象(Api Handler)
// 把每个 ApiHandler的路由注册给Root Router
func (c *IocContainter) RouteRegistry(r gin.IRouter) {
	// 找到被托管的APIHandler
	for _, obj := range c.store {
		// 判断这个对象是一个APIHandler对象
		if api, ok := obj.(GinApiHandler); ok {
			api.Registry(r)
		}
	}
}
