package main

import (
	"context"
	"fmt"
	"gitee.com/guohao88/blog/ioc"
	"gitee.com/guohao88/blog/protocol"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	//注册对象
	_ "gitee.com/guohao88/blog/apps"
	//tokenApiHandler "gitee.com/guohao88/blog/apps/token/api"
	//tokenImpl "gitee.com/guohao88/blog/apps/token/impl"
	//userImpl "gitee.com/guohao88/blog/apps/user/impl"
	"gitee.com/guohao88/blog/conf"
)

func main() {
	//1. 加载配置
	err := conf.LoadConfigToml("conf/config.toml")
	if err != nil {
		log.Panicln(err)
	}
	//2. 初始化控制
	// 2.1 user controller
	//userServiceImpl := userImpl.NewUserServiceImpl()
	// 2.2 token controller
	//tokenServiceImpl := tokenImpl.NewTokenServiceImpl(userServiceImpl)
	//  通过Ioc来完成依赖的装载, 完成了依赖的倒置(ioc 依赖对象注册)
	if err := ioc.Controller().Init(); err != nil {
		panic(err)
	}
	// 2.3 token api handler

	// 初始化Api Handler
	if err := ioc.ApiHandler().Init(); err != nil {
		panic(err)
	}

	//3. 启动http协议服务器, 注册 handler路由
	//r := gin.Default()
	//ioc.ApiHandler().RouteRegistry(r.Group("/api/blog"))

	// 启动协议服务器
	//addr := conf.C().App.HttpAddr()
	//fmt.Printf("HTTP API监听地址: %s", addr)
	//for _, route := range r.Routes() {
	//	fmt.Printf("%s %s\n", route.Method, route.Path) // 使用 printf 替代 println，避免在路径中添加额外的换行符
	//}
	//
	//err = r.Run(addr)
	//fmt.Println(err)

	// 跑在后台的http server
	server := protocol.NetHttpServer()
	go func() {
		if err := server.Run(); err != nil {
			fmt.Println("blog", err)
		}
	}()
	// 处理信号量
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, syscall.SIGTERM, syscall.SIGINT, syscall.SIGHUP, syscall.SIGKILL, syscall.SIGQUIT)
	// 等待信号到来
	sn := <-ch
	fmt.Println(sn)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	server.Close(ctx)
	fmt.Println("清理工作")
}
