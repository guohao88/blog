package blog

import (
	"context"
	"encoding/json"
	"gitee.com/guohao88/blog/common"
	"strconv"
)

const (
	AppName = "blog"
)

// 定义博客模块接口
type Service interface {
	//创建博客
	CreateBlog(context.Context, *CreateBlogRequest) (*Blog, error)
	// 修改文章状态
	UpdateBlogStatus(context.Context, *UpdateBlogStatusRequest) (*Blog, error)
	//更新文章
	UpdateBlog(context.Context, *UpdateBlogRequest) (*Blog, error)
	//删除文章
	DeleteBlog(context.Context, *DeleteBlogRequest) error
	// 详情页, 尽量多的把关联的数据查询出来， content
	DescribeBlog(context.Context, *DescribeBlogRequest) (*Blog, error)
	// 查询文章列表, 列表查询, 没有必要查询文章的具体内容
	QueryBlog(context.Context, *QueryBlogRequest) (*BlogSet, error)
	// 文章审核, 审核通过的文章才能被看到
	AuditBlog(context.Context, *AuditBlogRequest) (*Blog, error)
}

type UpdateBlogStatusRequest struct {
	// 如果定义一篇文章, 使用对象Id, 具体的某一篇文章
	BlogID int64 `json:"blog_id"`
	// 修改的状态: DRAFT/PUBLISHED  发布前后都可以修改草稿
	Status Status `json:"status"`
}

func NewPatchUpdateBlogRequest(id string) *UpdateBlogRequest {
	return &UpdateBlogRequest{
		BlogId:            id,
		UpdateMode:        UPDATE_MODE_PATCH,
		CreateBlogRequest: NewCreateBlogRequest(),
	}
}

func NewPtuUpdateBlogRequest(id string) *UpdateBlogRequest {
	return &UpdateBlogRequest{
		BlogId:            id,
		UpdateMode:        UPDATE_MODE_PUT,
		CreateBlogRequest: NewCreateBlogRequest(),
	}
}

type UpdateBlogRequest struct {
	// 如果定义一篇文章, 使用对象Id, 具体的某一篇文章
	BlogId string `json:"blog_id"`
	// blog的范围, 不是用户传递进来的, 是api接口层 自动填充
	Scope *common.Scope `json:"scope"`
	// 更新方式 区分全量更新/部分更新 默认全量
	UpdateMode UpdateMode `json:"update_mode"`
	// 用户更新请求, 用户只传了个标签
	*CreateBlogRequest
}

func NewDescribeBlogRequest(id string) *DescribeBlogRequest {
	return &DescribeBlogRequest{
		BlogId: id,
	}
}

type DescribeBlogRequest struct {
	BlogId string `json:"blog_id"`
}

func NewDeleteBlogRequest(id string) *DeleteBlogRequest {
	return &DeleteBlogRequest{
		BlogId: id,
	}
}

type DeleteBlogRequest struct {
	// 如果定义一篇文章, 使用对象Id, 具体的某一篇文章
	BlogId string `json:"blog_id"`
}

func NewBlogSet() *BlogSet {
	return &BlogSet{
		Items: []*Blog{},
	}
}

type BlogSet struct {
	// 博客的总数
	Total int64 `json:"total"`
	// 返回的一页的数据
	Items []*Blog `json:"items"`
}

func (b *BlogSet) String() string {
	dj, _ := json.Marshal(b)
	return string(dj)
}

func (s *BlogSet) Add(items ...*Blog) {
	s.Items = append(s.Items, items...)
}

func NewQueryBlogRequest() *QueryBlogRequest {
	return &QueryBlogRequest{
		PageSize:   10,
		PageNumber: 1,
	}
}

// 后端分页
type QueryBlogRequest struct {
	// 页的大小
	PageSize int `json:"page_size"`
	// 当前处于几页
	PageNumber int `json:"page_number"`
	// 0 表示草稿状态, 要查询所有的博客
	// nil 没有这个过滤条件
	// 0   DRAFT
	// 1   PUBLISHED
	Status *Status `json:"status"`
}

// 依赖, 根据分页大小,当前页数 可以推导出 获取元素的开始和结束位置
// [1,2,3,4,5] [6,7,8,9,10] [11,12,13,14,15]
// limite(offset, limte) limite(5 * 0,5) [1,2,3,4,5]
// limite(5*1,5) [6,7,8,9,10]
// limite(5*2,5)  [11,12,13,14,15]
func (r *QueryBlogRequest) Offset() int {
	return int(r.PageSize * (r.PageNumber - 1))
}

func (r *QueryBlogRequest) ParsePageSize(ps string) {
	psInt, err := strconv.ParseInt(ps, 10, 64)
	if err != nil && psInt != 0 {
		r.PageSize = int(psInt)
	}
}

func (r *QueryBlogRequest) ParsePageNumber(pn string) {
	psInt, err := strconv.ParseInt(pn, 10, 64)
	if err != nil && psInt != 0 {
		r.PageNumber = int(psInt)
	}
}

func (r *QueryBlogRequest) SetStatus(s Status) {
	r.Status = &s
}

func NewAuditBlogRequest(id string) *AuditBlogRequest {
	return &AuditBlogRequest{
		BlogId: id,
	}
}

type AuditBlogRequest struct {
	// 审核的文章
	BlogId string `json:"blog_id"`
	// 是否审核成功
	IsAuditPass bool `json:"is_audit_pass"`
}
