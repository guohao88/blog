package api

import (
	"gitee.com/guohao88/blog/apps/blog"
	"gitee.com/guohao88/blog/apps/token"
	"gitee.com/guohao88/blog/apps/user"
	"gitee.com/guohao88/blog/middleware"
	"gitee.com/guohao88/blog/response"
	"github.com/gin-gonic/gin"
)

func (h *blogApiHandler) Registry(r gin.IRouter) {

	// r 是Gin的路由器
	v1 := r.Group("v1").Group("blogs")

	// 需要公开给访客访问 可以不用鉴权
	// GET /vblog/api/v1/blogs/
	v1.GET("/", h.QueryBlog)
	// GET /vblog/api/v1/blogs/43
	// url路径里面有变量: :id, 什么一个路径变量, r.Params("id") 43
	// /:id ---? /43   id = 43
	v1.GET("/:id", h.DescribeBlog)

	// 后台管理接口 需要认证
	v1.Use(middleware.NewTokenAuther().Auth) //以下都需要认证

	// POST /vblog/api/v1/blogs/  认证类似于以下
	// POST /vblog/api/v1/blogs/ -- HanlerFunc Chain: append(CreateBlog, middlewares.NewTokenAuther().Auth, ...)
	v1.POST("/", middleware.Required(user.ROLE_AUTHOR), h.CreateBlog) //创建博客需要ROLE_AUTHOR权限用户
	v1.PUT("/:id", middleware.Required(user.ROLE_AUTHOR), h.PtuUpdateBlog)
	v1.PATCH("/:id", middleware.Required(user.ROLE_AUTHOR), h.PatchUpdateBlog)
	v1.DELETE("/:id", middleware.Required(user.ROLE_AUTHOR), h.DeleteBlog)
	v1.POST("/:id/audit", middleware.Required(user.ROLE_AUTHOR), h.AuditBlog)
}

// 创建博客
func (h *blogApiHandler) CreateBlog(c *gin.Context) {
	// 从Gin请求上下文中: c.Keys, 获取认证过后的鉴权结果
	tkObj := c.Keys[token.TOKEN_GIN_KEY_NAME]
	tk := tkObj.(*token.Token)

	in := blog.NewCreateBlogRequest()
	err := c.BindJSON(in)
	if err != nil {
		response.Failed(c, err)
		return
	}
	// 充上下文中补充 用户信息·
	in.CreateBy = tk.UserName
	ins, err := h.svc.CreateBlog(c.Request.Context(), in)
	if err != nil {
		response.Failed(c, err)
		return
	}

	response.Success(c, ins)
}

// 列表查询
func (h *blogApiHandler) QueryBlog(c *gin.Context) {

	in := blog.NewQueryBlogRequest()
	in.ParsePageSize(c.Query("page_size"))
	in.ParsePageSize(c.Query("page_number"))
	switch c.Query("status") {
	case "draft":
		in.SetStatus(blog.STATUS_DRAFT) //=0
	case "published":
		in.SetStatus(blog.STATUS_PUBLISHED) //=1
	}
	set, err := h.svc.QueryBlog(c.Request.Context(), in)
	if err != nil {
		response.Failed(c, err)
	}
	response.Success(c, set)
	return
}

// 详情查询
func (h *blogApiHandler) DescribeBlog(c *gin.Context) {
	in := blog.NewDescribeBlogRequest(c.Param("id")) //id根据Param来
	ins, err := h.svc.DescribeBlog(c.Request.Context(), in)
	if err != nil {
		response.Failed(c, err)
	}
	response.Success(c, ins)
	return
}

// 全量更新
func (h *blogApiHandler) PtuUpdateBlog(c *gin.Context) {
	// 从Gin请求上下文中: c.Keys, 获取认证过后的鉴权结果
	tkObj := c.Keys[token.TOKEN_GIN_KEY_NAME]
	tk := tkObj.(*token.Token)

	in := blog.NewCreateBlogRequest()
	err := c.BindJSON(in)
	if err != nil {
		response.Failed(c, err)
		return
	}

	// 充上下文中补充 用户信息
	in.CreateBy = tk.UserName
	ins, err := h.svc.CreateBlog(c.Request.Context(), in)
	if err != nil {
		response.Failed(c, err)
		return
	}

	response.Success(c, ins)
}

// 部分更新
func (h *blogApiHandler) PatchUpdateBlog(c *gin.Context) {
	in := blog.NewPatchUpdateBlogRequest(c.Param("id")) //id根据Param来
	ins, err := h.svc.UpdateBlog(c.Request.Context(), in)
	if err != nil {
		response.Failed(c, err)
		return
	}
	if err != nil {
		response.Failed(c, err)
		return
	}
	response.Success(c, ins)
}

// 删除
func (h *blogApiHandler) DeleteBlog(c *gin.Context) {
	in := blog.NewDeleteBlogRequest(c.Param("id")) //id根据Param来
	err := h.svc.DeleteBlog(c.Request.Context(), in)
	if err != nil {
		response.Failed(c, err)
	}
	response.Success(c, "ok")
	return
}

// 审核该文章 是否合法
// POST /vblog/api/v1/blogs/43/audit
func (h *blogApiHandler) AuditBlog(c *gin.Context) {
	in := blog.NewAuditBlogRequest(c.Param("id"))
	err := c.BindJSON(in)
	if err != nil {
		response.Failed(c, err)
		return
	}

	ins, err := h.svc.AuditBlog(c.Request.Context(), in)
	if err != nil {
		response.Failed(c, err)
		return
	}

	response.Success(c, ins)
}
