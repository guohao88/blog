package api

import (
	"gitee.com/guohao88/blog/apps/blog"
	"gitee.com/guohao88/blog/ioc"
)

func init() {
	ioc.ApiHandler().Registry(&blogApiHandler{})
}

type blogApiHandler struct {
	// 依赖控制器
	svc blog.Service
}

func (t *blogApiHandler) Name() string {
	return blog.AppName
}
func (t *blogApiHandler) Init() error {
	t.svc = ioc.Controller().Get(blog.AppName).(blog.Service)
	return nil
}
