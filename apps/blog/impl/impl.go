package impl

import (
	"gitee.com/guohao88/blog/apps/blog"
	"gitee.com/guohao88/blog/conf"
	"gitee.com/guohao88/blog/ioc"
	"gorm.io/gorm"
)

func init() {
	ioc.Controller().Registry(&blogServiceImpl{})
}

type blogServiceImpl struct {
	// db
	db *gorm.DB
}

func (s *blogServiceImpl) Init() error {
	// db ioc
	s.db = conf.C().Mysql.GetConn().Debug()
	return nil
}
func (s *blogServiceImpl) Name() string {
	return blog.AppName
}
