package impl_test

import (
	"context"
	"gitee.com/guohao88/blog/apps/blog"
	"gitee.com/guohao88/blog/ioc"
	"gitee.com/guohao88/blog/test/test"
)

var (
	svc blog.Service
	ctx = context.Background()
)

func init() {
	test.DevelopmentSetup()
	// 依赖另一个实现类
	svc = ioc.Controller().Get(blog.AppName).(blog.Service)
}
