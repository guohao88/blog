package impl_test

import (
	"fmt"
	"gitee.com/guohao88/blog/apps/blog"
	"testing"
)

// 创建博客
func TestCreateBlog(t *testing.T) {
	in := blog.NewCreateBlogRequest()
	in.Title = "admin11"
	in.Content = "goland11"
	in.Tags["分类"] = "golang"
	ins, err := svc.CreateBlog(ctx, in)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

// 列表查询 查询全部
func TestQueryBlog(t *testing.T) {
	in := blog.NewQueryBlogRequest()
	set, err := svc.QueryBlog(ctx, in)
	if err != nil {
		t.Fatal(err)
	}
	//t.Log(set)
	fmt.Println("查询到的结果:", set)

}

// 详情查询单个
func TestDescribeBlog(t *testing.T) {
	in := blog.NewDescribeBlogRequest("41")
	set, err := svc.DescribeBlog(ctx, in)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

// 全量更新
func TestUpdateBlogPut(t *testing.T) {
	in := blog.NewPtuUpdateBlogRequest("41")
	in.Content = "goland-v2"
	ins, err := svc.UpdateBlog(ctx, in)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

// 部分更新
func TestUpdateBlogPatch(t *testing.T) {
	in := blog.NewPatchUpdateBlogRequest("41")
	in.Title = "测试4"
	in.Tags["分类"] = "Golang2"
	ins, err := svc.UpdateBlog(ctx, in)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestDeleteBlog(t *testing.T) {
	in := blog.NewDeleteBlogRequest("41")
	err := svc.DeleteBlog(ctx, in)
	if err != nil {
		t.Fatal(err)
	}
}
