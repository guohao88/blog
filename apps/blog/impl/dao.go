package impl

import (
	"context"
	"gitee.com/guohao88/blog/apps/blog"
	"gitee.com/guohao88/blog/common"
)

func (i *blogServiceImpl) update(ctx context.Context, scope *common.Scope, ins *blog.Blog) error {
	exec := i.db.
		WithContext(ctx).
		Where("id = ?", ins.Id)

	if scope != nil {
		if scope.UserId != "" {
			exec = exec.Where("create_by = ?", scope.UserId)
		}
	}

	return exec.
		Updates(ins).
		Error
}
