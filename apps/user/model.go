package user

import (
	"encoding/json"
	"gitee.com/guohao88/blog/common"
	"golang.org/x/crypto/bcrypt"
)

func NewUser(req *CreateUserRequest) *User {
	req.PasswordHash()
	return &User{
		Mtea:              common.NewMtea(),
		CreateUserRequest: req,
	}
}

// 对返回指针进行json化
// 返回指针类型 impl_test.go:23: &{0xc000011ab8 0xc000200a80}
// impl_test.go:23: {"id":10,"created_at":1714708473,"updated_at":1714708473,"userName":"root3","password":"do@u.can","role":1,"label":{}}
func (u *User) String() string {
	dj, _ := json.Marshal(u)
	return string(dj)
}

// 用于定于定义对象模型(存入数据库里面的对象)
type User struct {
	//通用信息
	*common.Mtea
	//用户传过来的请求
	*CreateUserRequest
}

// 判断该用户的密码是否正确
func (u *User) CheckPassword(password string) error {
	return bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
}
