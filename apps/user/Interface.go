package user

import (
	"context"
	"fmt"
	"golang.org/x/crypto/bcrypt"
)

const (
	AppName = "user"
)

func NewCreateUserRequest() *CreateUserRequest {
	return &CreateUserRequest{
		Role:  ROLE_AUDITOR,
		Label: map[string]string{},
	}
}

// 定义User包的能力 接口定义 站在使用放的角度来定义的
type Service interface {
	//创建用户
	CreateUser(context.Context, *CreateUserRequest) (*User, error) //第一个肯定是context，第二个是请求参数,第三个是返回参数
	//删除用户
	DeleteUser(context.Context, *DeleteUserRequest) error
	//查询用户
	DescribeUser(context.Context, *DescribeUserRequest) (*User, error)
}

// 请求对象，创建用户
type CreateUserRequest struct {
	UserName string `json:"userName" gorm:"column:username"` //用户需要通过接口传递，需要json //用户需要通过接口传递，需要json
	Password string `json:"password"`
	Role     Role   `json:"role"` //用户的角色
	// 对象标签, Dep:部门A
	// Label 没法存入数据库，不是一个结构化的数据
	// 比如就存储在数据里面 ，存储为Json, 需要ORM来帮我们完成(https://gorm.io/zh_CN/docs/serializer.html)json的序列化和存储
	// 直接序列化为Json存储到 lable字段
	Label    map[string]string `json:"label" gorm:"serializer:json"`
	isHashed bool
}

func NewDescribeUserRequestById(id string) *DescribeUserRequest {
	return &DescribeUserRequest{
		DescribeValue: id,
	}
}
func NewDescribeUserRequestByUsername(username string) *DescribeUserRequest {
	return &DescribeUserRequest{
		DescribeBy:    DESCRIBE_BY_USERNAME,
		DescribeValue: username,
	}
}

// 支持通过id和username来查询用户
type DescribeUserRequest struct {
	DescribeBy    DescribeBy `json:"describe_by"`
	DescribeValue string     `json:"describe_value"`
}

func (req *CreateUserRequest) Validate() error {
	if req.UserName == "" || req.Password == "" {
		return fmt.Errorf("用户名或者密码需要填写")
	}
	return nil
}

func (req *CreateUserRequest) SetIsHashed() {
	req.isHashed = true
}

func (req *CreateUserRequest) PasswordHash() {
	if req.isHashed {
		return
	}
	b, _ := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	req.Password = string(b)
	req.isHashed = true
}

// 请求对象，删除用户
type DeleteUserRequest struct {
	Id int `json:"id"`
}
