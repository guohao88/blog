package impl

import (
	"context"
	"gitee.com/guohao88/blog/apps/user"
	"gitee.com/guohao88/blog/conf"
	"gitee.com/guohao88/blog/exception"
	"gitee.com/guohao88/blog/ioc"
	"gorm.io/gorm"
)

// 导入这个包的时候, 直接把这个对象 UserServiceImpl 注册给Ioc
//
//	UserServiceImpl
//
// 注册User业务模块(业务模块的名称是user.AppName)的控制器
// User Service 的具体实现 &UserServiceImpl{} 注入
// 好处就是可以随时更换业务的具体实现
// 提出来 放到初始化里面 ioc
func init() {
	ioc.Controller().Registry(&UserServiceImpl{})
}

// 定义托管到Ioc里面的名称
func (i *UserServiceImpl) Name() string {
	return user.AppName
}

// 对比接口声明的约束对不对
var _ user.Service = &UserServiceImpl{}

//var _ user.Service = (*UserServiceImpl)(nil) //13写法

// db 怎么来?
// 通过配置 https://gorm.io/zh_CN/docs/index.html
// 定义ioc对象的初始化
func (i *UserServiceImpl) Init() error {
	//db对象通过配置获取
	i.db = conf.C().Mysql.GetConn()
	return nil
}

// 接口的实现
type UserServiceImpl struct {
	db *gorm.DB
}

// 创建用户
func (i *UserServiceImpl) CreateUser(
	ctx context.Context,
	req *user.CreateUserRequest) (
	*user.User, error) {
	//1.校验用户参数
	if err := req.Validate(); err != nil {
		return nil, err
	}
	//2.生成一个user对象
	ins := user.NewUser(req)

	// 3. 保存到数据库, ORM怎么知道这个对象保持在那个表里面, 怎么知道行应用如何对应
	//  ins如何往users表里面存
	// Create  ins.TableName(), orm提供的功能, 看orm的文档
	// gorm:"column:username"  通过struct tag 定义对象列映射关系

	if err := i.db.
		WithContext(ctx).
		Create(ins).
		Error; err != nil {
		return nil, err
	}
	//i.db.Create(ins)
	//4.返回结果
	return ins, nil
}

// 查询用户
func (i *UserServiceImpl) DescribeUser(
	ctx context.Context,
	req *user.DescribeUserRequest) (
	*user.User, error) {
	quser := i.db.WithContext(ctx)
	// 1. 构造我们的查询条件
	// 根据条件来构建Where语句

	// id= ? or username = ?
	switch req.DescribeBy {
	case user.DESCRIBE_BY_ID:
		quser = quser.Where("id = ?", req.DescribeValue)

	case user.DESCRIBE_BY_USERNAME:
		quser = quser.Where("username = ?", req.DescribeValue)
	}

	ins := user.NewUser(user.NewCreateUserRequest())
	if err := quser.First(ins).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, exception.NewNotFound("%s not found", req.DescribeValue)
		}
		return nil, err
	}
	// 数据库里面存储的就是Hash
	ins.SetIsHashed()

	return ins, nil
}

// 删除用户
func (i *UserServiceImpl) DeleteUser(
	ctx context.Context,
	req *user.DeleteUserRequest) error {
	return i.db.
		WithContext(ctx).
		Where("id = ?", req.Id).
		Delete(&user.User{}).
		Error
}
