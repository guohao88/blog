package impl_test

import (
	"context"
	"gitee.com/guohao88/blog/apps/user"
	"gitee.com/guohao88/blog/ioc"
	"gitee.com/guohao88/blog/test/test"
	"testing"
)

var (
	userSvc user.Service
	ctx     = context.Background()
)

func TestUserServiceImpl_CreateUser(t *testing.T) {
	req := user.NewCreateUserRequest()
	req.UserName = "admin"
	req.Password = "do@u.can"
	//req.Role = user.ROLE_AUTHOR
	u, err := userSvc.CreateUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(u)
}

func TestUserServiceImpl_DescribeUserRequestById(t *testing.T) {
	req := user.NewDescribeUserRequestById("14")
	ins, err := userSvc.DescribeUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}
func TestUserServiceImpl_DeleteUser(t *testing.T) {
	err := userSvc.DeleteUser(ctx, &user.DeleteUserRequest{
		Id: 6,
	})
	if err != nil {
		t.Fatal(err)
	}
}

func init() {
	test.DevelopmentSetup()
	// 取消对象: ioc.Controller().Get(user.AppName)
	// 断言为接口来使用(只使用对象接口提供出来的能力)
	userSvc = ioc.Controller().Get(user.AppName).(user.Service)
}
