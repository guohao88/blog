package user_test

import (
	"golang.org/x/crypto/bcrypt"
	"testing"
)

func TestBcrypto5Hash(t *testing.T) {
	b, _ := bcrypt.GenerateFromPassword([]byte("123456"), bcrypt.DefaultCost)
	t.Log(string(b))
}
