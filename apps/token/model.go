package token

import (
	"encoding/json"
	"fmt"
	"gitee.com/guohao88/blog/apps/user"
	"gitee.com/guohao88/blog/exception"
	"github.com/rs/xid"
	"time"
)

func NewToken() *Token {
	return &Token{
		// 生产一个UUID的字符串
		AccessToken:           xid.New().String(),
		AccessTokenExpiredAt:  7200,
		RefreshToken:          xid.New().String(),
		RefreshTokenExpiredAt: 3600 * 24 * 7,
		CreatedAt:             time.Now().Unix(),
	}
}

type Token struct {
	// 该Token是颁发
	UserId int `json:"user_id"`
	// 用户名称， user_name
	UserName string `json:"username" gorm:"column:username"`
	// 颁发给用户的访问令牌(用户需要携带Token来访问接口)
	AccessToken string `json:"access_token"`

	// 过期时间(2h), 单位是秒
	AccessTokenExpiredAt int `json:"access_token_expired_at"`
	// 刷新Token
	RefreshToken string `json:"refresh_token"`
	// 刷新Token过期时间(7d)
	RefreshTokenExpiredAt int `json:"refresh_token_expired_at"`

	// 创建时间
	CreatedAt int64 `json:"created_at"`
	// 更新实现
	UpdatedAt int64 `json:"updated_at"`
	// token状态
	Token_Status Token_status `json:"token_status"`
	// 办颁发Token时，用户的角色，重新登录才会生效  鉴权功能
	Role user.Role `json:"role" gorm:"-"`
}

// 对返回指针进行json化
func (tk *Token) String() string {
	dj, _ := json.Marshal(tk)
	return string(dj)
}

// 计算Token的过期时间
func (t *Token) ExpiredTime() time.Time {
	return time.Unix(t.CreatedAt, 0).
		Add(time.Duration(t.AccessTokenExpiredAt) * time.Second)
}
func (t *Token) IsExpired() error {
	duration := time.Since(t.ExpiredTime())
	expiredSeconds := duration.Seconds() //过期了多少秒
	if expiredSeconds > 0 {
		return exception.NewTokenExpired("token %s 过期了 %f秒",
			t.AccessToken, expiredSeconds)
	}

	return nil
}

// 获取token状态，如果为ok则返回token
func (t *Token) Token_status() (*Token, error) {
	switch t.Token_Status {
	case TOKEN_STATUS_EXPIRED:
		return nil, exception.NewTokenExpired(fmt.Sprintln("token状态已过期"))
	case TOKEN_STATUS_OK:
	}
	return t, nil
}
