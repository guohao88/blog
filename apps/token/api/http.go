package api

import (
	"gitee.com/guohao88/blog/apps/token"
	"gitee.com/guohao88/blog/ioc"
	"gitee.com/guohao88/blog/response"
	"github.com/gin-gonic/gin"
)

// ioc程序有两个阶段，第一对象注册阶段，第二对象初始化阶段
// 将tokenapi对象注册到ioc
func init() {
	ioc.ApiHandler().Registry(&TokenApiHandler{})
}

// 实现iocObj 接口Name
func (t *TokenApiHandler) Name() string {
	return token.AppName
}

// 实现iocobj接口，初始化
func (t *TokenApiHandler) Init() error {
	t.svc = ioc.Controller().Get(token.AppName).(token.Service)
	return nil
}

//func NewTokenApiHandler(tokenServiceImpl token.Service) *TokenApiHandler {
//	return &TokenApiHandler{
//		svc: tokenServiceImpl,
//	}
//}

// 不适用接口, 直接定义Gin的一个handlers
// 什么是Gin的Handler  HandlerFunc
// HandlerFunc defines the handler used by gin middleware as return value.
// type HandlerFunc func(*Context)
// HandleFunc 只是定义 如何处理 HTTP 的请求与响应

type TokenApiHandler struct {
	// 依赖控制器
	svc token.Service
}

// 需要把HandleFunc 添加到Root路由，定义 API ---> HandleFunc
// 可以选择把这个Handler上的HandleFunc都注册到路由上面
func (h *TokenApiHandler) Registry(r gin.IRouter) {

	// r 是Gin的路由器
	v1 := r.Group("v1")
	v1.POST("/tokens/", h.Login)
	v1.DELETE("/tokens/", h.Logout)
}

// Login HandleFunc
func (h *TokenApiHandler) Login(c *gin.Context) {
	// 1. 获取用户的请求参数， 参数在Body里面
	// 一定要使用JSON
	req := token.NewLoginRequest()

	// json.unmarsal
	// http boyd ---> LoginRequest Object
	err := c.BindJSON(req)
	if err != nil {
		response.Failed(c, err)
		return
	}

	// 2. 执行逻辑
	// 把http 协议的请求 ---> 控制器的请求
	ins, err := h.svc.Login(c.Request.Context(), req)
	if err != nil {
		response.Failed(c, err)
		return
	}

	// access_token 通过SetCookie 直接写到浏览器客户端(Web)
	c.SetCookie(token.TOKEN_COOKIE_NAME, ins.AccessToken, 0, "/", "localhost", false, true)

	// 3. 返回响应
	response.Success(c, ins)
	//c.JSON(http.StatusOK, ins)
}

// Logout HandleFunc
func (h *TokenApiHandler) Logout(*gin.Context) {
}
