package impl

import (
	"context"
	"gitee.com/guohao88/blog/apps/token"
	"gitee.com/guohao88/blog/apps/user"
	"gitee.com/guohao88/blog/conf"
	"gitee.com/guohao88/blog/exception"
	"gitee.com/guohao88/blog/ioc"
	"gorm.io/gorm"
	"strconv"
)

// 注册将对象注册到ioc
func init() {
	ioc.Controller().Registry(&TokenServiceImpl{})
}

// // 实现iocObj 接口Name
func (i *TokenServiceImpl) Name() string {
	return token.AppName
}

// 对比接口声明的约束对不对
// var _ token.Token = &TokenServiceImpl{}
// // 实现iocobj接口，初始化
func (i *TokenServiceImpl) Init() error {
	i.db = conf.C().Mysql.GetConn().Debug()
	i.user = ioc.Controller().Get(user.AppName).(user.Service) //通过ioc获取user依赖
	return nil
	//user: userSvcImpl,
}

type TokenServiceImpl struct {
	// db
	db *gorm.DB
	// 依赖User模块, 直接操作user模块的数据库(users)?
	// 这里需要依赖另一个业务领域: 用户管理领域
	user user.Service
}

// 登录接口(颁发Token)
func (i *TokenServiceImpl) Login(
	ctx context.Context, req *token.LoginRequest) (
	*token.Token, error) {
	// 1. 查询用户
	uReq := user.NewDescribeUserRequestByUsername(req.Username)
	u, err := i.user.DescribeUser(ctx, uReq)
	if err != nil {
		if exception.IsNotFound(err) {
			return nil, token.AuthFailed
		}
		return nil, err
	}
	//2.对比密码
	err = u.CheckPassword(req.Password)
	if err != nil {
		return nil, token.AuthFailed
	}
	// 3. 颁发token
	tk := token.NewToken()
	tk.UserId = u.Id
	tk.UserName = u.UserName

	// 4. 保存Token orm用发
	if err := i.db.
		WithContext(ctx).
		Create(tk).
		Error; err != nil {
		return nil, err
	}

	// 避免同一个用户多次登录
	// 4. 颁发成功后  把之前的Token标记为失效
	return tk, nil
}

// 校验Token 是给内部中间层使用 身份校验层
// 校验完后返回Token, 通过Token获取 用户信息
func (i *TokenServiceImpl) ValiateToken(
	ctx context.Context,
	req *token.ValiateToken) (
	*token.Token, error) {
	//1、查询token
	tk := token.NewToken()
	err := i.db.WithContext(ctx).
		Where("access_token = ?", req.AccessToken). //判断token是否正确
		First(tk).Error
	if err != nil {
		return nil, err
	}
	//判断token是否过期
	if err := tk.IsExpired(); err != nil {
		return nil, err
	}
	// 补充用户信息，只补充了用户角色
	u, err := i.user.DescribeUser(ctx, user.NewDescribeUserRequestById(strconv.Itoa(tk.UserId)))
	if err != nil {
		return nil, err
	}
	tk.Role = u.Role
	return tk, nil
}

// 退出接口(销毁Token)
func (i *TokenServiceImpl) Logout(
	ctx context.Context, req *token.LogoutRequest) error {
	// 1、获取token对象
	tk := token.NewToken()
	// 2、根据用户传入的accesstoken值到数据库中查到指定记录
	err := i.db.WithContext(ctx).Where("access_token = ?", req.AccessToken).First(&tk).Error
	if err != nil {
		return err
	}
	// 3、获取token的状态，如果为ok则返回token对象，
	newtk, err := tk.Token_status()
	if err != nil {
		return exception.New(505, err.Error())
	}
	// 4、将token的状态设置为不可用
	newtk.Token_Status = token.TOKEN_STATUS_EXPIRED
	// 5、报错新状态进数据库
	err = i.db.WithContext(ctx).Model(newtk).
		Where("access_token=?", tk.AccessToken).
		Update("token_status", tk.Token_Status).Error
	if err != nil {
		return err
	}
	return nil
}
