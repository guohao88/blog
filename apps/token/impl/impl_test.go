package impl_test

import (
	"context"
	"fmt"
	"gitee.com/guohao88/blog/apps/token"
	"gitee.com/guohao88/blog/ioc"
	"gitee.com/guohao88/blog/test/test"
	"testing"
)

var (
	tokenSvc token.Service
	ctx      = context.Background()
)

func TestLogin(t *testing.T) {
	req := token.NewLoginRequest()
	req.Username = "admin"
	req.Password = "do@u.can"
	tk, err := tokenSvc.Login(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func TestValiateToken(t *testing.T) {
	req := token.NewValiateToken("cossfgriuhulck3lu6ag")
	tk, err := tokenSvc.ValiateToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func TestTokenImpl_Logout(t *testing.T) {
	err := tokenSvc.Logout(ctx, &token.LogoutRequest{
		AccessToken: "cossfgriuhulck3lu6ag",
	})
	if err != nil {
		t.Fatal(err)
	}

}

func init() {
	test.DevelopmentSetup()
	// 依赖另一个实现类
	tokenSvc = ioc.Controller().Get(token.AppName).(token.Service)
	fmt.Println("————————————————", tokenSvc)
}
