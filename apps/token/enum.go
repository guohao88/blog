package token

import "gitee.com/guohao88/blog/exception"

type Token_status int

const (
	// token正常
	TOKEN_STATUS_OK Token_status = iota
	// token过期
	TOKEN_STATUS_EXPIRED
)
const (
	TOKEN_COOKIE_NAME  = "access_token"
	TOKEN_GIN_KEY_NAME = "access_token"
)

var (
	CookieNotFound = exception.NewAuthFailed("cookie %s not found", TOKEN_COOKIE_NAME)
)
