package middleware

import (
	"gitee.com/guohao88/blog/apps/token"
	"gitee.com/guohao88/blog/apps/user"
	"gitee.com/guohao88/blog/exception"
	"gitee.com/guohao88/blog/ioc"
	"gitee.com/guohao88/blog/response"
	"github.com/gin-gonic/gin"
	"net/http"
)

func NewTokenAuther() *TokenAuther {
	return &TokenAuther{
		tk: ioc.Controller().Get(token.AppName).(token.Service),
	}
}

// 用于认证的中间件
// 用于Token鉴权的中间件
type TokenAuther struct {
	tk   token.Service
	role user.Role
}

// 怎么鉴权
func (a *TokenAuther) Auth(c *gin.Context) {
	// 1. 获取Token
	at, err := c.Cookie(token.TOKEN_COOKIE_NAME)
	if err != nil {
		if err == http.ErrNoCookie {
			response.Failed(c, token.CookieNotFound)
			return
		}
		response.Failed(c, err)
		return
	}

	// 2.调用Token模块来认证
	in := token.NewValiateToken(at)
	tk, err := a.tk.ValiateToken(c.Request.Context(), in)
	if err != nil {
		response.Failed(c, err)
		return
	}

	// 把鉴权后的 结果: tk, 放到请求的上下文, 方便后面的业务逻辑使用
	if c.Keys == nil {
		c.Keys = map[string]any{}
	}
	c.Keys[token.TOKEN_GIN_KEY_NAME] = tk //校验token是否合法
}

// 权限鉴定，鉴权是在用户已经完成认证的情况下进行的
// 判断当前用户的角色
func (a *TokenAuther) Perm(c *gin.Context) {
	// 1. 获取Token
	tkObj := c.Keys[token.TOKEN_COOKIE_NAME]
	if tkObj == nil {
		response.Failed(c, exception.NewPermissionDenied("token not found"))
		return
	}
	tk, ok := tkObj.(*token.Token)
	if !ok {
		response.Failed(c, exception.NewPermissionDenied("token not an *token.Token"))
		return
	}
	//fmt.Printf("user %s role %v\n", tk.UserName, tk.Role)
	//如果是admin直接放行
	if tk.Role == user.ROLE_ADMIN {
		return
	}
	//判断是不是要求的角色
	if tk.Role != a.role {
		response.Failed(c, exception.NewPermissionDenied("role %v not allow", tk.Role))
	}
}

// 携带参数的gin的中间件
func Required(role user.Role) gin.HandlerFunc {
	a := NewTokenAuther()
	a.role = role
	return a.Perm
}
