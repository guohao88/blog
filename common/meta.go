package common

import "time"

// 使用构造函数进行Mtea对象初始化，防止空指针
// 默认的参数，指定一些对象默认参数
func NewMtea() *Mtea {
	return &Mtea{
		CreatedAt: time.Now().Unix(),
	}
}

// 通用信息
type Mtea struct {
	//在添加数据库需要定义ID
	Id int `json:"id"`
	//用户创建时间
	CreatedAt int64 `json:"created_at"`
	//更新时间
	UpdatedAt int64 `json:"updated_at"`
}

// 控制用户访问数据的访问
// 操作数据的时候, 加上一个where条件
// 比如用户A10, 要去编辑用户B(12)的文章,  id=10 and create_by = 10
type Scope struct {
	UserId string `json:"user_id"`
}
