import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    // 登录页
    {
      path: '/login',
      name: 'Login',
      component: () => import('../views/login/LoginView.vue')
    },
    // 前台
    {
      path: '/frontend',
      name: 'FrontendLayout',
      component: () => import('../views/frontend/LayoutView.vue'),
      children: [
        {
          // /blogs
          // blogs --> /frontend/blogs
          path: 'blogs',
          name: 'FrontendBlogs',
          component: () => import('../views/frontend/blog/ListView.vue')
        }
      ]
    },
    // 后台
    {
      path: '/backend',
      name: 'BackendLayout',
      component: () => import('../views/backend/LayoutView.vue'),
      redirect: {name: 'BackendBlog'},
      children: [
        {
          // blogs
          // blogs --> /frontend/blogs
          path: 'blogs',
          name: 'BackendBlog',
          component: () => import('../views/backend/blog/ListView.vue')
        },
        {
          // /blogs
          // blogs --> /frontend/blogs
          path: 'blogs_edit',
          name: 'BlogEdit',
          component: () => import('../views/backend/blog/EditView.vue')
        },
        {
          // /blogs
          // blogs --> /frontend/blogs
          path: 'comment',
          name: 'Comment',
          component: () => import('../views/comment/ListPage.vue')
        },
      ]
    }
  ]
})
// 补充路由的页面导航守卫
// router.beforeEach(beforeEach)
export default router
