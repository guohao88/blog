import client from './client'


// 区分API请求, 可以全大写
// config?: AxiosRequestConfig<D>
// Get 请求, params =  url=a=1&b2
// 封装成一个函数
export var LIST_BLOG = (params) => client.get('/api/blog/v1/blogs/',{ params })