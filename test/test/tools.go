package test

import (
	//注册对象
	_ "gitee.com/guohao88/blog/apps"
	"gitee.com/guohao88/blog/conf"
	"gitee.com/guohao88/blog/ioc"
)

func DevelopmentSetup() {
	err := conf.LoadConfigFromEnv()
	if err != nil {
		panic(err)
	}

	// 对象的初始化
	if err := ioc.Controller().Init(); err != nil {
		panic(err)
	}
}
