CREATE TABLE `user` (
                        `id` int NOT NULL AUTO_INCREMENT COMMENT '用户id',
                        `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
                        `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户密码',
                        `create_at` varchar(255) NOT NULL COMMENT '创建时间',
                        `update_at` varchar(255) NOT NULL COMMENT '更新时间',
                        `role` tinyint NOT NULL COMMENT '角色：0普通，1是管理员',
                        `label` varchar(255) NOT NULL COMMENT '用户标签',
                        `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '用户手机号',
                        `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '用户地址',
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `inx_username` (`username`) COMMENT '用户唯一'
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `token` (
                         `user_id` int NOT NULL,
                         `username` varchar(255) NOT NULL,
                         `access_token` varchar(255) NOT NULL,
                         `access_token_expried_at` int NOT NULL,
                         `refresh_token` varchar(255) NOT NULL,
                         `refresh_token_expried_at` int NOT NULL,
                         `created_at` int NOT NULL,
                         `updated_at` int NOT NULL,
                         `token_status` tinyint NOT NULL,
                         PRIMARY KEY (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `blogs` (
                         `id` int NOT NULL AUTO_INCREMENT COMMENT '文章id',
                         `title` varchar(255) NOT NULL COMMENT '文章标题',
                         `author` varchar(255) NOT NULL COMMENT '文章作者',
                         `created_at` varchar(255) NOT NULL COMMENT '文章创建时间',
                         `updated_at` varchar(255) NOT NULL COMMENT '文章更新时间',
                         `published_at` varchar(255) NOT NULL COMMENT '文章发布时间',
                         `summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '文章概要描述',
                         `content` longtext NOT NULL COMMENT '文章内容',
                         `status` tinyint NOT NULL COMMENT '文章当前状态',
                         `tags` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标签',
                         `created_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
                         `audit_at` varchar(255) NOT NULL COMMENT '审核时间',
                         `is_audit_pass` tinyint NOT NULL COMMENT '是否审核通过',
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `idx_title` (`title`) COMMENT 'title添加唯一键约束'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;