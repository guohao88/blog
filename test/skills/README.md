# 关于记录的补充

这个是Skill 包, 所有的测试代码 以一个go module 的方式组织

## 单元测试，具备Debug能力(入门)

测试分类:
+ 单元测试/Debug测试(代码调试) add(x,y) add_test(xxxx)
+ 集成测试, 多个项目进行联合调试(微服务项目)


关于单元测试:
+ 功能调试: Debug一个功能
+ 程序调试: 程序启动起来, 以Debug方式运行


### 如何编译单元测试

+ 单元测试的文件命名: add---> add_test:   文件名_test.go
+ 单元测试函数的定义:
+ 函数的名称: Test功能名称 TestSum
+ 函数的参数: testing.T
+ 函数返回: 无

1. 功能调试(run test)
```go
func TestSum(t *testing.T) {
// print
// 如果打印不出来值, 需要调整ide设置 test Flag的设置
// "go.testFlags": [
// 你的单元测试和代码都修改, 单元测试会被缓存
//     "-count=1",
// 打印单元测试过程中的详细日志 fmt.xxx
//     "-v"
// ],
t.Log(unittest.Sum(1, 2))
}
```

debug 调试: 断点调试
+ 断点必须打在 有代码的地方
+ Continue: 到下一个断点
+ Step Over: 下一步, 跳过当前这步
+ Step In: 当步调试, 进入执行的函数内部


### 程序调试

调试的是整个程序, 一般是由Ide, 调试是程度的独立运行, 也是需要有相应配置

debug配置:
```json
{
// 使用 IntelliSense 了解相关属性。
// 悬停以查看现有属性的描述。
// 欲了解更多信息，请访问: https://go.microsoft.com/fwlink/?linkid=830387
"version": "0.2.0",
"configurations": [
{
"name": "Skills Run",
"type": "go",
"request": "launch",
"mode": "auto",
// 运行你程序的命令: go run main.go
// program 你需要配置的入口文件的位置
"program": "${workspaceFolder}/skills/main.go"
}
]
}
```

## Context(请求上下文)

### 请求的取消(Goroutine的退出)
context.WithTimeout  超时3s程序自动退出
```go
ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
defer cancel()

err := mycontext.Curl(ctx, "http://httpbin.org/delay/5")
if err != nil {
t.Fatal(err)
}
```

### 数据的传递

请求的上下文数据传递
```go
// 1. 用户认证 Login() tk ---> 用户的身份
// 2. 转账 Payment()
func Payment(
//Login
//tk -->admin
//
ctx context.Context, tk string) error {
//请求上文
ctx = context.WithValue(ctx, Username{}, "admin")
ctx = context.WithValue(ctx, Password{}, "123456")
//下文
DoPayment(ctx)
return nil
}

```

1. 业务数据传递: 用户身份
2. 非业务: traceID:   Login ---> Payment ---> DB ---> ...

```
context.Background.WithValue(type context.Username, val admin).WithValue(type context.Password, val 123456)
```