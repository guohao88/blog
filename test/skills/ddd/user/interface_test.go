package user_test

import (
	"context"
	"fmt"
	user2 "gitee.com/guohao88/blog/test/skills/ddd/user"
	"testing"
)

func TestService(t *testing.T) {
	var svc user2.Service

	svc = &UserserviceImpl{}
	svc.CreateUser(
		context.Background(),
		&user2.CreateUserRequest{})
}

// 比如在功能没有实现之前，可以先用Mock实现这个接口行不行
// 是一个Mock实现(测试用例里面使用)
type UserserviceImpl struct {
}

func (i *UserserviceImpl) CreateUser(
	context.Context, *user2.CreateUserRequest) (
	*user2.User,
	error) {
	// 具体怎么做
	Name := user2.CreateUserRequest{"admin", "123456"}
	fmt.Println(Name)
	return nil, nil
}
