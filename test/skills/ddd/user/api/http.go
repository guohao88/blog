package api

import (
	"gitee.com/guohao88/blog/test/skills/ddd/user"
	"github.com/gin-gonic/gin"
)

// 利用构造函数防止程序为空跑不了
func NewHandler(svc user.Service) *Handler {
	return &Handler{
		svc: svc,
	}
}

//定义对象，就负责实现接口暴露(Restful)

type Handler struct {
	svc user.Service // 依赖一个svc实现来(一个接口来实现)
}

// 实现 CreateUser接口
func (h *Handler) CreateUser(c *gin.Context) {
	h.svc.CreateUser(c.Request.Context(), &user.CreateUserRequest{})
}
