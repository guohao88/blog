package impl

import (
	"context"
	user2 "gitee.com/guohao88/blog/test/skills/ddd/user"
)

var (
	// 就是一个约束条件, 不会产生内存, 编译器帮你做类型检查,不对这段就报错
	_ user2.Service = (*Impl)(nil)
)

// 定义构造器
func NewImpl() *Impl {
	return &Impl{}
}

// impl 他就是来实现 User Service 业务
type Impl struct{}

func (i *Impl) CreateUser(
	context.Context, *user2.CreateUserRequest) (
	*user2.User,
	error) {
	// 具体怎么做
	return nil, nil
}
