package user

import (
	"context"
)

type Service interface {
	// 创建用户
	CreateUser(context.Context, *CreateUserRequest) (*User, error) //第一个肯定是context，第二个是请求参数,第三个是返回参数
}

type CreateUserRequest struct {
	UserName string
	Password string
}
