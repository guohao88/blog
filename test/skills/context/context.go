package context

import (
	"context"
	"fmt"
	"io"
	"net/http"
)

// ctx 都是函数的第一个参数
func Curl(ctx context.Context, url string) error {
	client := http.DefaultClient
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}

	//携带上下文
	req = req.WithContext(ctx)
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	fmt.Println(body)
	return nil
}

// 请求的上下文数据传递
// 1. 用户认证 Login() tk ---> 用户的身份
// 2. 转账 Payment()

type Username struct{}
type Password struct{}

func Payment(
	//Login
	//tk -->admin
	//
	ctx context.Context, tk string) error {
	//请求上文
	ctx = context.WithValue(ctx, Username{}, "admin")
	ctx = context.WithValue(ctx, Password{}, "123456")
	//下文
	DoPayment(ctx)
	return nil
}

func DoPayment(ctx context.Context) {
	fmt.Println(ctx.Value(Username{}))
	fmt.Println(ctx.Value(Password{}))
	fmt.Println(ctx)
}
