package context_test

import (
	"context"
	mycontext "gitee.com/guohao88/blog/test/skills/context"
	"testing"
	"time"
)

func TestCurl(t *testing.T) {

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	//err := mycontext.Curl(ctx, "http://httpbin.org/delay/5")
	err := mycontext.Curl(ctx, "https://baidu.com/")
	if err != nil {
		t.Fatal(err)
	}
}

func TestPayment(t *testing.T) {
	ctx := context.Background()
	err := mycontext.Payment(ctx, "xxx")
	if err != nil {
		t.Fatal(err)
	}
}
