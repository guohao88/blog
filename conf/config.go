package conf

import (
	"encoding/json"
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"sync"
)

// 程序默认配置
func DefaultConfig() *Config {
	return &Config{
		Mysql: &Mysql{
			Host: "192.168.10.102",
			//Host:     "192.168.99.213",
			Port:     3306,
			DB:       "blog",
			Password: "do@u.can",
			Username: "root",
		},
		App: &App{
			HttpHost: "127.0.0.1",
			HttpPort: 8080,
		},
	}
}

// 维护整个程序的配置
type Config struct {
	Mysql *Mysql `json:"mysql" toml:"mysql"`
	App   *App   `json:"app" toml:"app"`
}

// 对返回指针进行json化，类似于
// conf_test.go:13: &{0xc00008c360}
// conf_test.go:13: {"mysql":{"host":"192.168.10.102","port":3306,"database":"blog","password":"do@u.can","username":"root"}}
func (c *Config) String() string {
	dj, _ := json.Marshal(c)
	return string(dj)
}

type Mysql struct {
	Host     string `json:"host" toml:"host" env:"MYSQL_HOST"`
	Port     int    `json:"port" toml:"port" env:"MYSQL_PORT"`
	DB       string `json:"database" toml:"database" env:"MYSQL_DB"`
	Password string `json:"password" toml:"password" env:"MYSQL_PASSWORD"`
	Username string `json:"username" toml:"username" env:"MYSQL_USERNAME"`

	// 缓存一个对象
	lock sync.Mutex `:"lock"` //加锁,避免多个goroutine同时操作
	conn *gorm.DB   `:"conn"`
}

type App struct {
	HttpHost string `json:"http_host" env:"HTTP_HOST"`
	HttpPort int64  `json:"http_port" env:"HTTP_PORT"`
}

func (a *App) HttpAddr() string {
	return fmt.Sprintf("%s:%d", a.HttpHost, a.HttpPort)
}

// 参考 https://github.com/go-sql-driver/mysql#dsn-data-source-name 获取详情
// dsn := "user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local"
func (m *Mysql) DSN() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		m.Username,
		m.Password,
		m.Host,
		m.Port,
		m.DB,
	)
}

// 返回一个数据库链接
func (m *Mysql) GetConn() *gorm.DB {
	m.lock.Lock()
	defer m.lock.Unlock() //解锁

	// 在进行m.conn = conn 赋值操作时 由锁存在不会冲突
	//https://gorm.io/zh_CN/docs/connecting_to_the_database.html
	if m.conn == nil {
		conn, err := gorm.Open(mysql.Open(m.DSN()), &gorm.Config{})
		if err != nil {
			panic(err)
		}
		m.conn = conn
	}
	return m.conn
}
