package protocol

import (
	"context"
	"fmt"
	"gitee.com/guohao88/blog/conf"
	"gitee.com/guohao88/blog/ioc"
	"github.com/gin-gonic/gin"
	http "net/http"
)

func NetHttpServer() *HttpServer {
	r := gin.Default()
	ioc.ApiHandler().RouteRegistry(r.Group("/api/blog"))
	return &HttpServer{
		server: &http.Server{
			Addr:    conf.C().App.HttpAddr(), // 服务监听的地址
			Handler: r,                       // 监听关联的路由处理
		},
	}
}

// 封装一个自己的http server
type HttpServer struct {
	server *http.Server
}

// 打印监听地址
func (h *HttpServer) Run() error {
	fmt.Println("listen addr:", conf.C().App.HttpAddr())
	return h.server.ListenAndServe()
}

// 优雅关闭
func (h *HttpServer) Close(ctx context.Context) {
	h.server.Shutdown(ctx)
	fmt.Println("blog 停止")
}
